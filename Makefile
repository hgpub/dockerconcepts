all: build

build: simpleimage helloworld helloworld2

run: run_simpleimage run_helloworld run_helloworld2

.PHONY: simpleimage helloworld helloworld2 run_simpleimage run_helloworld run_helloworld2

simpleimage:
	docker build -t simpleimage simpleimage

helloworld:
	docker build -t helloworld helloworld

helloworld2:
	docker build -t helloworld2 helloworld2

yacreader:
	docker build -t yacreader yacreader

run_simpleimage:
	docker run --rm --name simpleimage_container simpleimage

run_helloworld:
	docker run --rm --name helloworld_container helloworld

run_helloworld2:
	docker run --rm --name helloworld2_container helloworld2

run_yacreader:
	xhost local:docker
	docker run --rm -i --net=host --env=DISPLAY --volume=/tmp/.X11-unix:/tmp/.X11-unix --volume=$(HOME)/lectura/comics:/comics yacreader
